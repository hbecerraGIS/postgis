#Unir al tiempo más cercano sensores

CREATE TABLE public.resultado AS
SELECT DISTINCT ON (tiempo_m) "no2 (ppb)", "voc (ppb)", "pm 10 (ug/m3)", "pm 2.5 (ug/m3)", "no2 (plume aqi)", "voc (plume aqi)", "pm 10 (plume aqi)", "pm 2.5 (plume aqi)", "pm 1 (ug/m3)", "pm 1 (plume aqi)", "tiempo_m", tiempo_p, latitude, longitude
FROM mediciones
CROSS JOIN posiciones
ORDER BY tiempo_m, tiempo_m <-> tiempo_p;
